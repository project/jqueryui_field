SUMMARY - Jquery UI Field
=============================

Jqueryui_field is a very simple Drupal module to use within the field system. module is providing the jqueryui field widget where Tabs & Accordion Can be displayed to the end users.


Installation:
-------------

Install this module as usual. Please see
http://drupal.org/documentation/install/modules-themes/modules-8


Usage:
------

Add jqueryui_field from the available field type dropdown & manage the display of the field.
Currently, this module is supporting the below field formeters.

Jqueryui Tabs
Jqueryui Accordion

Support:
--------
https://www.drupal.org/u/vedprakash
